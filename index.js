        $(document).ready(function() {
            //Инцилизируем selec2
            alert("test")
            $("#selectprioritet").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
        
        
            // идентификатор элемента (например: #datetimepicker1), для которого необходимо инициализировать виджет Bootstrap DateTimePicker
            $('#datetimepickerplan').datetimepicker({
                    locale: 'ru',
                    format : 'DD.MM.GGGG'
                });
                $('#datetimepickerfact').datetimepicker({
                    locale: 'ru',
                    format : 'DD.MM.GGGG'
                });
            
        //При нажаите на кнопку Подобрать идет открытие карты
        $("#sendForm").click(function() {
            //Проверка заполнения поленй основных
            //addAlert
            var locatedText = $("#region").find("select").val()
            var dataIn = $("#datetimepickerplan").find("input").val()
            var dataOut = $("#datetimepickerfact").find("input").val()
            //console.log(dataIn + " "  + dataOut)
            if(locatedText == "-"){
                //addAlert('#region', ' Необходимо выбрать направление' );
                return;
            }
            if(dataIn == ""){
                addAlert('datetimepickerplan input')
                //$("#datetimepickerplan input").attr("placeholder", "Необходимо заполнить поле");
                //$('#datetimepickerplan input').addClass('allertPlaceholder');
                return;
            };
            if(dataOut == ""){
                addAlert('datetimepickerfact input')
                //$("#datetimepickerfact input").attr("placeholder", "Необходимо заполнить поле");
                //$('#datetimepickerfact input').addClass('allertPlaceholder');
                return;
            }
            
            showMap(locatedText,dataIn,dataOut)
        })
        
        
        
            
        });
        
        

        
        function showMap(locatedText,dataIn,dataOut) {
                var updateformaDataIn = toDate(dataIn);
                var updateformaDataOut = toDate(dataOut);
                var deltaTime = (updateformaDataOut - updateformaDataIn) / 86400000;
                
                var arrayData = deltaDataInOut(dataIn,dataOut);
                deltaTime += 1;

                
                //Закрываем форму 
                $("#formAction").hide()
                //console.log("куда - "+ locatedText + " Сколько дней - " + deltaTime)
                
                
                //Вставляем в селектор #rangeTracing кол-во закладок (deltaTime)
                //Кол-во закладок не более 12 дней
                if (deltaTime >= 11) {
                    deltaTime = 11
                }

                console.log("После обновления количестов дней" + deltaTime)

                //Объект с вариантами готовых маршрутов
                var creatWay = [
                    {
                        id : "1",
                        city : "Самара",
                        name : "Названия тура",
                        type : "Активный....",
                        description : "Описание маршрута/ограничение",
                        time : "Время маршрута",
                        totalSumm : "подвопросом",
                        image : "Ссылка на картинку",
                        points : [{}],
                        /*points [//Какие точки будут входить в маршрут или месте из таблицы Excell (Самара)
                            {	//
                                id : "1",
                                name : "Остановка 1"
                                located : "Адресс",
                                timeStart : "Время начала",
                                timeFinish : "Время окончание",
                                summ : "350",
                                sale : "скидка -5%"
                                
                            },
                            
                        ]*/

                    }
                ]
                
                
                
                var place = [
    /////////////////////////////////////////////////////////////////////////////Самара			
                    {
                        id:1,
                        place : "Самара",
                        center : [53.195538, 50.101783],
                        points : [
                        {
                            id : 0,
                            name: "Театры",
                            style: "islands#blueFoodIcon",
                            icon: "",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                {
                                    id: "1",
                                    title: "Ресторан 2",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "",
                                    rating : "",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                            
                            ]
                        },
                        {
                            id : 1,
                            name: "Музеи",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-university",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 2,
                            name: "Cмотровые площадки",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-binoculars",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        
                        
                        
                        
                        {
                            id : 3,
                            name: "Зоопарк",
                            style: "islands#blueFoodIcon",
                            icon: "fab fa-sticker-mule",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 4,
                            name: "Цирк",
                            style: "islands#blueFoodIcon",
                            icon: "fab fa-sticker-mule",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 5,
                            name: "Дельфинарий океанариумы",
                            style: "islands#blueFoodIcon",
                            icon: "fab fa-sticker-mule",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 6,
                            name: "Кинотеатры",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-film",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                {
                                    id: "1",
                                    title: "Бар 2",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "",
                                    rating : "",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                            
                            ]
                        },
                        {
                            id : 7,
                            name: "Рестораны",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-utensils",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 8,
                            name: "Кафе/Кофейни",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-coffee",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 9,
                            name: "Бары и пабы",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-beer",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 10,
                            name: "Клубы",
                            style: "islands#blueFoodIcon",
                            icon: "fas fa-music",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 11,
                            name: "Парки аттракционов",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 12,
                            name: "Аквапарки",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 13,
                            name: "Боулинг-клубы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 14,
                            name: "Стадионы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        ]
                        
                    },
    /////////////////////////////////////////////////////////////////////////////Тюмень
                    {
                        id:2,
                        place : "Тюмень",
                        center: [57.153033, 65.534328],
                        points : [
                        {
                            id : 0,
                            name: "Театры",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                {
                                    id: "1",
                                    title: "Ресторан 2",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "",
                                    rating : "",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                            
                            ]
                        },
                        {
                            id : 1,
                            name: "Музеи",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 2,
                            name: "Зоопарк",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 3,
                            name: "Цирк",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 4,
                            name: "Дельфинарий океанариумы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Ресторан 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 5,
                            name: "Кинотеатры",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                {
                                    id: "1",
                                    title: "Бар 2",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "",
                                    rating : "",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                            
                            ]
                        },
                        {
                            id : 6,
                            name: "Рестораны",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 7,
                            name: "Кафе",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        {
                            id : 8,
                            name: "Кофейни",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 9,
                            name: "Бары и пабы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 10,
                            name: "Клубы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 11,
                            name: "Кальян бары",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 12,
                            name: "Парки аттракционов",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 13,
                            name: "Аквапарки",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 14,
                            name: "Боулинг-клубы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },{
                            id : 15,
                            name: "Стадионы",
                            style: "islands#blueFoodIcon",
                            items: [
                                {
                                    id: "0",
                                    title: "Бар 1",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    located: "Самара",
                                    center: [53.195538, 50.101783],
                                    sale : "5",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                                
                            
                            ]
                        },
                        ]	
                        
                        
                    },
    /////////////////////////////////////////////////////////////////////////////Казань	
                    {
                        id:3,
                        place : "Казань",
                        center: [55.796289, 49.108795],
                        points : [
                        {
                            id : 0,
                            name: "Рестораны/Бары/Кафе/Клубы",
                            style: "islands#blueFoodIcon",
                            iconcategory : "",
                            items: [
                                {
                                    id: "0",
                                    title: "SHVEIK (Швейк)",
                                    type: "Ресторан-пивоварня",
                                    locatedDes: "ул. Победы, 109 (пересечение с ул. Краснодонской, 16)",
                                    description: "Чешское пиво «Дукат», которое варится здесь же, в пивоварне, из отборного солода и хмеля по классическому старинному рецепту чешских пивоваров, конечно же такая же добротная рулька или хрустящие гренки, а так-же изумительные кнедлики и классическое для всех пивных ресторанов свиное колено. Средний чек - от 1200 руб.",
                                    timeWork: "ежедневно с 12:00 до 00:00",
                                    email: "shvake@restoria.ru",
                                    tel: "8 (846) 995-20-80",
                                    web: "http://www.restoria.ru/contacts-restaurant-ushveyka-8.html",
                                    center: [55.796289, 49.108795],
                                    sale : "",
                                    rating : "2",
                                    image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                                },
                            
                            ]	
                        },
                        ]
                        
                    },
                ]
                
                
                //console.log(place)
                // Группы объектов
                var groups2 = [
                    {
                        id : 1,
                        name: "Рестораны/Бары/Кафе/Клубы",
                        style: "islands#blueFoodIcon",
                        items: [
                                {
                                    id: "1",
                                    title: "SHVEIK (Швейк)",
                                    center: [50.426475, 30.563024],

                                    
                                },
                                
                        ]
                    },
                    {	
                        id : 2,
                        name: "Покушайки",
                        style: "islands#greenIcon",
                        items: [
                            {
                                id: 1,
                                center: [50.426472, 30.563022],
                                title: "Казанский Государственный Цирк",
                                rating : "3",
                                description : "",
                                located: "55.798852, 49.100432",
                                locatedDes : "площадь Тысячелетия, 2, Казань",
                                type : "Цирк",
                                timeWork : "круглосуточно",
                                sale : "5",
                                email : "vsbabenkov@sberbank.ru",
                                tel : "+79296056614",
                                web : "https://yandex.ru",
                                image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                            },
                        ]},
                    {	
                        id : 3,
                        name: "Оригинальные музейчики",
                        style: "islands#orangeIcon",
                        items: [
                            {
                                id: 1,
                                center: [50.426472, 30.563022],
                                title: "Казанский Государственный Цирк",
                                rating : "2.0",
                                description : "",
                                located: "55.798852, 49.100432",
                                locatedDes : "площадь Тысячелетия, 2, Казань",
                                type : "Цирк",
                                timeWork : "круглосуточно",
                                sale : "5",
                                email : "vsbabenkov@sberbank.ru",
                                tel : "+79296056614",
                                web : "https://yandex.ru",
                                image : "https://static.tildacdn.com/tild6262-3938-4535-b464-303337643639/______.jpg"
                            },
                        ]},
                ]
                //Пробегаемся по точкам отсорировываем по городу() и и по категориям type  
                
                //Фультруем по выбору города и дальше вытаскиваем метки
                var placeFilter =  filterArray(place,locatedText)

                for(var i=0; i<deltaTime; i++) {
                    addNewTab(i,arrayData)
                
                }

                function addNewTab(i,arrayData ){
                    var bookmarkMenuHTML = '';
                    var bookmarkContentHTML = '';
                    console.log((i + 1) + "Дней")
                    if (i == 0 ) {
                        bookmarkMenuHTML += '<li style="top: -25px;"><a style="color:black" href="#tabs'+i+'">'+arrayData[i]+'</a></li>'
                    // bookmarkContentHTML += '<div id="tabs'+i+'">'+
                    //                         '	<div id="trash" class="ui-widget-content ui-state-default"></div>'+
                    //                         '</div>'
                    } else {
                        bookmarkMenuHTML += '<li style="top: -25px;"><a style="color:black" href="#tabs'+i+'">'+arrayData[i]+'</a></li>'
                    
                    }
                    
                    console.log(bookmarkMenuHTML)
                    $rowConteiner = $( //'<div id="categoryLabel"><h4 class="ui-widget-header">Выбирите категорию:</h4></div>'+
                                                '<div class="row">'+
                                                    '<div class="container" style="display: inherit;">'+
                                                    // <!--Карта-->
                                                       
                                                        //<!--Список точек-->
                                                       // '<div id="listitems" class="col-sm-3">'+
                                                            //<!--Вставляем картинки категорий-->
                                                            //'<div id="categoryaPoint"></div>'+
                                                            
                                                            //<!--Вставляем Точки/Партнеры-->

                                                        '</div>'+
                                                    '</div>'+
                                                '</div>');                    
                                                        
                    $tabDiv = $('<div id="tabs'+i+'"></div>');
                    $mapDiv =  $('<div stle="height:500px;" id="map'+i+'" class="col-sm-9 map"></div>');
                    $trashDiv = $('<div id="trash'+i+'" style="background:white;" class="trash ui-widget-content ui-state-default"></div>')
                    $galleryUl = $('<ul id="gallery'+i+'" class="rootGallery gallery ui-helper-reset ui-helper-clearfix" style="width: 100%;padding:0px"></ul>');
                    $categoryaPoints     = $('<div id="categoryaPoints'+i+'">'+
                                                    '<div id="categoryLabel"><h4 class="ui-widget-header">Выбирите категорию:</h4></div>'+
                                                    '<div id="category"></div>'+
                                                '</div>');

                    
                    $rowConteiner.find(".container").append($mapDiv);
                    $rowConteiner.find(".container").append($galleryUl);
                    $tabDiv.append($trashDiv)
                    $tabDiv.append($categoryaPoints)
                    $tabDiv.append($rowConteiner)
                    // добавляем в DOM
                    $("#rangeTracing #tabs").children("ul").append(bookmarkMenuHTML);
                    $("#rangeTracing #tabs").append($tabDiv);

                     groups=   placeFilter 
                    // инициализируем карту
                    var myMap = new ymaps.Map( $mapDiv.attr("id") , {
                        center: groups.center,
                        zoom: 14,
                        controls: []
                    })
                    
                    // Контейнер для меню.
                    $menu = $('<ul id="menu" style="padding:0px"/>');
                    $galleryUl.append($menu);
                    for (var k = 0, l = groups.points.length; k < l; k++) {
                        createMenugroups(groups.points[k]);
                    }
                    function createMenugroups (groups) {
                        // Пункт меню.
                        var groopId = groups.id
                        var groupsIcon = groups.style
                        //Категория

                        //Добавляем категории ввиде картинок
                        
                        
                        var catItems ="";
                        var categoryaHTML = '<div class="categoryItems" id="group_'+groups.id+'">'+
                                            ' 		<div>'+
                                            '			<div id="iconCategory">'+
                                            '				<i style="width : 30px;height : 30px;" class="'+groups.icon+'"></i>'+
                                            '			</div>'+
                                            '			<div style="position:relative">'+
                                            '				<span id="categoryCount">'+groups.items.length+'</span>'+
                                            '			</div>'+
                                            '		</div>'+
                                            ' 		<div id="titleCategory">'+ groups.name +'</div>'+
                                            ' </div>'
                        
                        
                        catItems += categoryaHTML;
                        $categoryaPoints.find("#category").append(catItems)
                        catItems = ''
                        //var menuItem = $('<a class="titleListItem" id="group_'+groups.id+'" style="display:none"><li></li></a>'),
                        var menuItem = $('<a class="titleListItem" id="group_'+groups.id+'" ><li></li></a>'),
                        
                        
                        
                        
                        
                        
                        // Коллекция для геообъектов группы.
                            collection = new ymaps.GeoObjectCollection(null, { preset: groups.style }),
                        // Контейнер для подменю.
                            submenu = $('<ul id="gallery'+ i+'" class="gallery subGallery ui-helper-reset ui-helper-clearfix"/>');
                            
                        // Добавляем коллекцию на карту.
                        myMap.geoObjects.add(collection);
                        
                        
                        console.log("------------")
                        console.log(groups)
                        //Поправить сначало закрывать а потом показывать
                        $("#tabs"+i+ " #categoryaPoints"+i+" #group_"+groups.id).bind('click', function () {
                                    console.log("Таб "+ i + " -  " + this)
                                if (collection.getParent()) {
                                    myMap.geoObjects.remove(collection);
                                    menuItem.hide();                                    
                                    
                                } else {
                                    $("#menu .titleListItem").hide();
                                    myMap.geoObjects.add(collection);
                                    menuItem.show();
                                    
                                }
                        })
                        console.log($("#trash"+i+" #group_"+groups.id))
                            
                        
                        // Добавляем подменю.
                        menuItem
                            .append(submenu)
                            // Добавляем пункт в меню.
                            .appendTo(menu)
                            // По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
                            .find('li')
                            /*.bind('click', function () {
                                if (collection.getParent()) {
                                    myMap.geoObjects.remove(collection);
                                    submenu.hide();
                                } else {
                                    myMap.geoObjects.add(collection);
                                    submenu.show();
                                }
                            });*/
                        for (var j = 0, m = groups.items.length; j < m; j++) {
                            createSubMenu(groups.items[j], collection, submenu, groopId, groupsIcon);
                        }
                    }
                    function createSubMenu (item, collection, submenu, groupid, groupsIcon) {                         
                            

                            //Проверяем есть ли отдельные параметры
                            var IsSaleHTML = ''
                            if ((item.sale != "") || (item.sale == "0")) {
                                IsSaleHTML =  '	<div>'+
                                            '		<span style="position: absolute;background: #EF9A9A;color: white;border-radius: 5px;padding: 2px;font-size: 11px;">скидка: -'+item.sale+'%</span>'+
                                            '	</div>'
                            }
                            var ratingStar = rating(item.rating)
                            var IsRating = ''
                            //Проверяем на наличие рейтинга
                            if ((item.rating != "") || (item.rating == "0")) {
                            IsRating 	=	'			<div style="display:block;height: 18px;position:relative;text-align:left;">'+
                                                    ratingStar +
                                                '			</div>'	
                            }
                            
                            
                            var submenuItem = '<li class="ui-widget-content ui-corner-tr" id="group_'+groupid +'_'+ item.id+'">'+
                                                    '		<div id="fotoPoint">'+
                                                    '			<img src="'+item.image+'" style="position: relative;">'+
                                                    '		</div>' + 
                                                    '		<div id="titlePoint">'+
                                                    '			<div>'+
                                                    '				<h5 class="ui-widget-header">'+item.title+'</h5>'+
                                                    '			</div>'+
                                                    '			<div style="display:inline-block">'+
                                                                    IsRating + IsSaleHTML +
                                                    '			</div>'+
                                                    '		</div>'+
                                                    
                                                    '		<div id=infoPoint>'+
                                                    '			<div style="display:block;">'+
                                                    '				<div id="icondetail"><i class="fas fa-map-marker-alt" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                                    '				<div id="adressPointText">'+item.locatedDes+'</div>' +
                                                    '			</div>'+								 
                                                    '			<div style="width="100%;display:block">'+
                                                    '				<div id="icondetail"><i class="far fa-clock" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                                    '				<div id="workTimeText">'+item.timeWork+'</div>' +
                                                    '			</div>'+
                                                    '			<div style="width="100%;display:block">'+
                                                    '				<div id="icondetail"><i class="fas fa-phone-volume" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                                    '				<div id="workTelnumber"><a style="float:left" href="tel:"'+item.tel+'">'+item.tel+'</a></div>' + 
                                                    '			</div>'+
                                                    '		</div>' + 
                                                    '		<div style="width: 45%;float:left;text-align:left">'+
                                                    '			<button title="Подробнее" class="ui-icon ui-icon-detail" style="background-image:none;text-align:center">'+
                                                    '				<div onclick="viewLargerImage(\''+item.title+'\', \''+item.description+'\', \''+item.locatedDes+'\', \''+item.type+'\', \''+item.timeWork+'\', \''+item.sale+'\', \''+item.email+'\', \''+item.tel+'\', \''+item.web+'\', \''+item.image+'\')"><i style="text-align:center" class="fas fa-info-circle"></i></div>'+
                                                    '			</button>'+
                                                    '		</div>'+
                                                    '		<div style="width: 45%;float:right;text-align:right" id="rightButton"><button title="Добавить в маршрут" class="ui-icon ui-icon-add" style="background-image:none;text-align:center">+</button></div>' + 
                                                    '</li>'
                                
                                //Информация которая при нажатие на метку
                                
                                
                                
                                
                                
                                // Создание макета содержимого балуна.
                        // Макет создается с помощью фабрики макетов с помощью текстового шаблона.
                        BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                                    '<div style="width:250px" id="itemsBallon">'+
                                    '	<div style="display:inline-block;width:30%"><img style="width:95%" src="'+item.image+'" /></div>'+
                                    '		<div id="titlePoint">'+
                                    '			<div>'+
                                    '				<h5 class="ui-widget-header">'+item.title+'</h5>'+
                                    '			</div>'+
                                    '			<div style="display:inline-block">'+
                                                    IsRating + IsSaleHTML +
                                    '			</div>'+
                                    '		</div>'+
                                    '		<div id=infoPoint style="font-size: 0.9em;line-height: initial;">'+
                                    '			<div style="display:block;">'+
                                    '				<div id="icondetail"><i class="fas fa-map-marker-alt" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="adressPointText">'+item.locatedDes+'</div>' +
                                    '			</div>'+								 
                                    '			<div style="width="100%;display:block">'+
                                    '				<div id="icondetail"><i class="far fa-clock" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="workTimeText">'+item.timeWork+'</div>' +
                                    '			</div>'+
                                    '			<div style="width="100%;display:block">'+
                                    '				<div id="icondetail"><i class="fas fa-phone-volume" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="workTelnumber"><a style="float:left" href="tel:"'+item.tel+'">'+item.tel+'</a></div>' + 
                                    '			</div>'+
                                    '		</div>' + 
                                    '		<div style="float:right">'+
                                    '			<button title="Добавить в маршрут" class="ui-icon ui-icon-add" style="background-image:none;text-align:center">+</button>'+
                                    '		</div>'+
                                    '</div>', {

                                // Переопределяем функцию build, чтобы при создании макета начинать
                                // слушать событие click
                                build: function () {
                                    
                                    BalloonContentLayout.superclass.build.call(this);
                                    var itemHTML = this
                                    $('.ui-icon-add').bind('click', function(){
                                        contentBallon(itemHTML)
                                    })
                                },

                                // Аналогично переопределяем функцию clear, чтобы снять
                                // прослушивание клика при удалении макета с карты.
                                clear: function () {
                                    var itemHTML = this
                                    $('.ui-icon-add').unbind('click', function(){
                                        contentBallon(itemHTML)
                                        
                                    })
                                    BalloonContentLayout.superclass.clear.call(this);
                                },
                                
                        });
                                
                                //Функция добавления всего контента 
                                function contentBallon(item) {
                                    console.log(item)
                                    //Весь контент HTML
                                    itemHTML = item['_element'].childNodes[0]
                                    console.log($(itemHTML).html())
                                }
                                
                                
                                    
                                    
                                var placemark = new ymaps.Placemark(item.center, {}, {
                                    
                                    balloonContentLayout: BalloonContentLayout,
                                    // Запретим замену обычного балуна на балун-панель.
                                    // Если не указывать эту опцию, на картах маленького размера откроется балун-панель.
                                    balloonPanelMaxMapArea: 0
                                    
                                });
                                    
                            
                            



                            
                            collection.add(placemark);
                            $(submenuItem)
                                .appendTo(submenu)
                                // При клике по пункту подменю открываем/закрываем баллун у метки.
                                //.find('a')
                                
                                .bind('click', function () {
                                    if (!placemark.balloon.isOpen()) {
                                        placemark.balloon.open();
                                    } else {
                                        placemark.balloon.close();
                                    }
                                    return false;
                                });

                            var pointColor = groupsIcon.match(/#[a-z]+/)[0]
                            var pointHoverColor = groupsIcon.replace(pointColor,"#black")

                            //console.log(pointColor)
                            //console.log(groupsIcon.match(/#[a-z]+/));

                            //Функция изменяет метку при выборе
                            placemark.events
                                .add(['mouseenter', 'mouseleave'], function (e) {
                                
                                var target = e.get('target'),
                                    type = e.get('type');
                                    // Событие произошло на геообъекте.
                                    if (type == 'mouseenter') {
                                        target.options.set('preset', pointHoverColor);
                                    } else {
                                        target.options.set('preset', groupsIcon);
                                    }
                            });

                        }
                        
                        
                };


                //Добавляем закладки
                $("#textInOut").append("г. "+locatedText + " ("+ dataIn + " - " + dataOut + ")");

                //Инициализация табов
                $( "#rangeTracing #tabs" ).tabs();

                //Показываем шаблоны маршрутов
                 $("#mapShow").show();
                 //Исправляем глюк со скрытием делаем открытый первый элемент
                //Закрываем все меню
                $('#menu .titleListItem').hide();
                $('#menu #group_0').show();
                //$('#category .categoryItems:first').css("color","#4CAF50");
                //$('#category .categoryItems:first svg').css("color","#4CAF50");
                
                activeUI();
                console.log("Активация карты ///");
                ymaps.ready(function(){
                    console.log("Активация карты");
                    return
                }); //initMaps(placeFilter)
    }
        
        
        
    function initMaps(groups) {
        //Генирируем готовые шаблоны
        console.log(groups)

        // для каждого дня формируем новую карту
        
        $(".map").each(function(index,e){ 
            var elementId = $(e).attr("id")
            
            //для групп
            var catItems =  '' //Создаем
            var catelentID = elementId.replace("map","")
            // Создание экземпляра карты.
            console.log(groups.center)
            var myMap = new ymaps.Map(elementId , {
                center: groups.center,
                zoom: 14,
                controls: []
            })
            // Контейнер для меню.
            menu = $('<ul id="menu" style="padding:0px"/>');
            menu.appendTo($('.gallery'));
            for (var i = 0, l = groups.points.length; i < l; i++) {
                createMenugroups(groups.points[i]);
            }
            
            
            function createMenugroups (groups) {
                // Пункт меню.
                var groopId = groups.id
                var groupsIcon = groups.style
                //Категория

                //Добавляем категории ввиде картинок
                
                
                
                var categoryaHTML = '<div class="categoryItems" id="group_'+groups.id+'">'+
                                    ' 		<div>'+
                                    '			<div id="iconCategory">'+
                                    '				<i style="width : 30px;height : 30px;" class="'+groups.icon+'"></i>'+
                                    '			</div>'+
                                    '			<div style="position:relative">'+
                                    '				<span id="categoryCount">'+groups.items.length+'</span>'+
                                    '			</div>'+
                                    '		</div>'+
                                    ' 		<div id="titleCategory">'+ groups.name +'</div>'+
                                    ' </div>'
                
                
                catItems += categoryaHTML
                $("#categoryaPoints"+catelentID+" #category").append(catItems)
                catItems = ''
                //var menuItem = $('<a class="titleListItem" id="group_'+groups.id+'" style="display:none"><li></li></a>'),
                var menuItem = $('<a class="titleListItem" id="group_'+groups.id+'" ><li></li></a>'),
                
                
                
                
                
                
                // Коллекция для геообъектов группы.
                    collection = new ymaps.GeoObjectCollection(null, { preset: groups.style }),
                // Контейнер для подменю.
                    submenu = $('<ul id="gallery'+ index+'" class="gallery subGallery ui-helper-reset ui-helper-clearfix"/>');
                    
                // Добавляем коллекцию на карту.
                myMap.geoObjects.add(collection);
                
                
                console.log("------------")
                console.log(groups)
                //Поправить сначало закрывать а потом показывать
                $("#tabs"+catelentID+ " #categoryaPoints"+catelentID+" #group_"+groups.id).bind('click', function () {
                            console.log("Таб "+ catelentID + " -  " + this)
                        if (collection.getParent()) {
                            myMap.geoObjects.remove(collection);
                            menuItem.hide();
                            
                            
                        } else {
                            $("#menu .titleListItem").hide();
                            myMap.geoObjects.add(collection);
                            menuItem.show();
                            
                        }
                })
                console.log($("#trash"+catelentID+" #group_"+groups.id))
                    
                
                // Добавляем подменю.
                menuItem
                    .append(submenu)
                    // Добавляем пункт в меню.
                    .appendTo(menu)
                    // По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
                    .find('li')
                    /*.bind('click', function () {
                        if (collection.getParent()) {
                            myMap.geoObjects.remove(collection);
                            submenu.hide();
                        } else {
                            myMap.geoObjects.add(collection);
                            submenu.show();
                        }
                    });*/
                for (var j = 0, m = groups.items.length; j < m; j++) {
                    createSubMenu(groups.items[j], collection, submenu, groopId, groupsIcon);
                }
            }
            
            myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange:true}).then(function(){ if(myMap.getZoom() > 10) myMap.setZoom(10);});
            //Добавляем в закладку и обнуляем
            console.log($("#categoryaPoints"+catelentID+" #category").append(catItems))
            catItems = ''

            
        })


        function createSubMenu (item, collection, submenu, groupid, groupsIcon) {
            
            

            //Проверяем есть ли отдельные параметры
            var IsSaleHTML = ''
            if ((item.sale != "") || (item.sale == "0")) {
                IsSaleHTML =  '	<div>'+
                            '		<span style="position: absolute;background: #EF9A9A;color: white;border-radius: 5px;padding: 2px;font-size: 11px;">скидка: -'+item.sale+'%</span>'+
                            '	</div>'
            }
            var ratingStar = rating(item.rating)
            var IsRating = ''
            //Проверяем на наличие рейтинга
            if ((item.rating != "") || (item.rating == "0")) {
            IsRating 	=	'			<div style="display:block;height: 18px;position:relative;text-align:left;">'+
                                    ratingStar +
                                '			</div>'	
            }
            
            
            var submenuItem = '<li class="ui-widget-content ui-corner-tr" id="group_'+groupid +'_'+ item.id+'">'+
                                    '		<div id="fotoPoint">'+
                                    '			<img src="'+item.image+'" style="position: relative;">'+
                                    '		</div>' + 
                                    '		<div id="titlePoint">'+
                                    '			<div>'+
                                    '				<h5 class="ui-widget-header">'+item.title+'</h5>'+
                                    '			</div>'+
                                    '			<div style="display:inline-block">'+
                                                    IsRating + IsSaleHTML +
                                    '			</div>'+
                                    '		</div>'+
                                    
                                    '		<div id=infoPoint>'+
                                    '			<div style="display:block;">'+
                                    '				<div id="icondetail"><i class="fas fa-map-marker-alt" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="adressPointText">'+item.locatedDes+'</div>' +
                                    '			</div>'+								 
                                    '			<div style="width="100%;display:block">'+
                                    '				<div id="icondetail"><i class="far fa-clock" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="workTimeText">'+item.timeWork+'</div>' +
                                    '			</div>'+
                                    '			<div style="width="100%;display:block">'+
                                    '				<div id="icondetail"><i class="fas fa-phone-volume" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                                    '				<div id="workTelnumber"><a style="float:left" href="tel:"'+item.tel+'">'+item.tel+'</a></div>' + 
                                    '			</div>'+
                                    '		</div>' + 
                                    '		<div style="width: 45%;float:left;text-align:left">'+
                                    '			<button title="Подробнее" class="ui-icon ui-icon-detail" style="background-image:none;text-align:center">'+
                                    '				<div onclick="viewLargerImage(\''+item.title+'\', \''+item.description+'\', \''+item.locatedDes+'\', \''+item.type+'\', \''+item.timeWork+'\', \''+item.sale+'\', \''+item.email+'\', \''+item.tel+'\', \''+item.web+'\', \''+item.image+'\')"><i style="text-align:center" class="fas fa-info-circle"></i></div>'+
                                    '			</button>'+
                                    '		</div>'+
                                    '		<div style="width: 45%;float:right;text-align:right" id="rightButton"><button title="Добавить в маршрут" class="ui-icon ui-icon-add" style="background-image:none;text-align:center">+</button></div>' + 
                                    '</li>'
                
                //Информация которая при нажатие на метку
                
                
                
                
                
                // Создание макета содержимого балуна.
        // Макет создается с помощью фабрики макетов с помощью текстового шаблона.
        BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="width:250px" id="itemsBallon">'+
                    '	<div style="display:inline-block;width:30%"><img style="width:95%" src="'+item.image+'" /></div>'+
                    '		<div id="titlePoint">'+
                    '			<div>'+
                    '				<h5 class="ui-widget-header">'+item.title+'</h5>'+
                    '			</div>'+
                    '			<div style="display:inline-block">'+
                                    IsRating + IsSaleHTML +
                    '			</div>'+
                    '		</div>'+
                    '		<div id=infoPoint style="font-size: 0.9em;line-height: initial;">'+
                    '			<div style="display:block;">'+
                    '				<div id="icondetail"><i class="fas fa-map-marker-alt" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                    '				<div id="adressPointText">'+item.locatedDes+'</div>' +
                    '			</div>'+								 
                    '			<div style="width="100%;display:block">'+
                    '				<div id="icondetail"><i class="far fa-clock" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                    '				<div id="workTimeText">'+item.timeWork+'</div>' +
                    '			</div>'+
                    '			<div style="width="100%;display:block">'+
                    '				<div id="icondetail"><i class="fas fa-phone-volume" style="color:#c2c2c2;width:25px;height:15px;"></i></div>'+
                    '				<div id="workTelnumber"><a style="float:left" href="tel:"'+item.tel+'">'+item.tel+'</a></div>' + 
                    '			</div>'+
                    '		</div>' + 
                    '		<div style="float:right">'+
                    '			<button title="Добавить в маршрут" class="ui-icon ui-icon-add" style="background-image:none;text-align:center">+</button>'+
                    '		</div>'+
                    '</div>', {

                // Переопределяем функцию build, чтобы при создании макета начинать
                // слушать событие click
                build: function () {
                    
                    BalloonContentLayout.superclass.build.call(this);
                    var itemHTML = this
                    $('.ui-icon-add').bind('click', function(){
                        contentBallon(itemHTML)
                    })
                },

                // Аналогично переопределяем функцию clear, чтобы снять
                // прослушивание клика при удалении макета с карты.
                clear: function () {
                    var itemHTML = this
                    $('.ui-icon-add').unbind('click', function(){
                        contentBallon(itemHTML)
                        
                    })
                    BalloonContentLayout.superclass.clear.call(this);
                },
                
        });
                
                //Функция добавления всего контента 
                function contentBallon(item) {
                    console.log(item)
                    //Весь контент HTML
                    itemHTML = item['_element'].childNodes[0]
                    console.log($(itemHTML).html())
                }
                
                
                    
                    
                var placemark = new ymaps.Placemark(item.center, {}, {
                    
                    balloonContentLayout: BalloonContentLayout,
                    // Запретим замену обычного балуна на балун-панель.
                    // Если не указывать эту опцию, на картах маленького размера откроется балун-панель.
                    balloonPanelMaxMapArea: 0
                    
                });
                    
            
            



            
            collection.add(placemark);
            $(submenuItem)
                .appendTo(submenu)
                // При клике по пункту подменю открываем/закрываем баллун у метки.
                //.find('a')
                
                .bind('click', function () {
                    if (!placemark.balloon.isOpen()) {
                        placemark.balloon.open();
                    } else {
                        placemark.balloon.close();
                    }
                    return false;
                });

            var pointColor = groupsIcon.match(/#[a-z]+/)[0]
            var pointHoverColor = groupsIcon.replace(pointColor,"#black")

            //console.log(pointColor)
            //console.log(groupsIcon.match(/#[a-z]+/));

            //Функция изменяет метку при выборе
            placemark.events
                .add(['mouseenter', 'mouseleave'], function (e) {
                
                var target = e.get('target'),
                    type = e.get('type');
                    // Событие произошло на геообъекте.
                    if (type == 'mouseenter') {
                        target.options.set('preset', pointHoverColor);
                    } else {
                        target.options.set('preset', groupsIcon);
                    }
            });

        }
            
        
        //Исправляем глюк со скрытием делаем открытый первый элемент
        //Закрываем все меню
        $('#menu .titleListItem').hide();
        $('#menu #group_0').show();
        //$('#category .categoryItems:first').css("color","#4CAF50");
        //$('#category .categoryItems:first svg').css("color","#4CAF50");
        
        activeUI();
        
            
        
        
        }
        
        //Фильтруем города из формы
        function filterArray (array,located) {
                    var newObj = {} //Объект
                    $.each(array, function(){
                        if (this.place == located) {
                            newObj.title = this.place
                            newObj.center = this.center
                            newObj.points = (this.points) 
                            //nerArray.push(array)
                            
                            //newArray = (this.points)
                            //nerArray.push(this.center) //Кладем центр
                            //nerArray.push(newArray)	
                        }
                        
                    });
                    //console.log(newObj)
                    return newObj
        }
        
        //JqueryUI
        function viewLargerImage(title,description,locatedDes,type,timeWork,sale,email,tel,web,image) {

                var infoPointHTML = 
                    '<div class="modal fade" id="openModalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'+
                    '  <div class="modal-dialog" role="document" style="width: 100%;height: auto;margin: 0 auto;">'+
                    '    <div class="modal-content">'+
                    '      <div class="modal-header">'+
                    '		 <img style="width:100%;border-radius: 5px;" src="'+image+'"/>'+
                    '        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 5px;font-size: 47px;color: white;right: 20px;"><span aria-hidden="true">&times;</span></button>'+
                    '		 <div id="pointTitle"><h5 style="font-size:">'+title+'</h5></div>'
    if((sale != "") || (sale != "0")) {
    infoPointHTML +='		 <div id="saleSpan"><span>скидка: -'+sale+'%</span></div>'
    }
    infoPointHTML +='      </div>'+
                    '      <div class="modal-body">'+
                            '<div class="container">'+
                                '<div class="row">'+
                    '				<div></div>'+
                                '</div>'+
                            '</div>'+
                    '      </div>'+
                    '    </div>'+
                    '  </div>'+
                    '</div>';
                $("body").append(infoPointHTML)
                $("#openModalInfo").modal('show');
                
                $('#openModalInfo').on('hidden.bs.modal', function (e) {
                    $('#openModalInfo').remove();
                })
            


        }
        
        function activeUI() {
        // There's the gallery and the trash
        var $gallery = $( ".gallery" ),
            $trash = $( ".trash" );
        
        // Let the gallery items be draggable
        $( "li", $gallery ).draggable({
            cancel: "button.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: "document",
            helper: "clone",
            cursor: "pointer"
            
        });
        
        
        $trash.droppable({
            accept: ".gallery > li",
            classes: {
                "ui-droppable-active": "ui-state-highlight"
            },
            drop: function( event, ui ) {
                addImage( ui.draggable );
            }
        });
        
        $gallery.droppable({
            accept: ".trash0 li",
            classes: {
                "ui-droppable-active": "ui-state-highlight"
            },
            drop: function( event, ui ) {
                deleteImage( ui.draggable );
                
            }
        });
        
        function trashdroppable(text){
            console.log(text)
        }
        
        
        //Функция удаления точки из тура
        var trash_icon = "<button title='Удалить точку' class='ui-icon ui-icon-del' style='background-image:none;text-align:center'>х</button>";
        function deleteImage( $item ) {
                //var trashId = $item.parent().attr("id");
                var trashId = $item.context.parentElement.parentElement.id
                $item.fadeOut(function() {$item.find("#fotoPoint").css({"width" : "30%"})});
                $item.fadeOut(function() {
                    $item
                        .find("button.ui-icon-del").remove()
                        .end()
                        .find("#rightButton").append(add_icon)
                        .end()
                        .find( "img" )
                        .end()
                        .fadeIn();
                        //  определяем группу и добавляем элемент в нее
                        var itemId = $item[0].id			
                        var groupId = itemId.slice(0, itemId.lastIndexOf("_")); //Определение к какой группе объект
                        
                        
                        var galleryId = trashId.replace("trash","gallery");					
                        $(".rootGallery").each(function(i,e){ 
                            if(e.id == galleryId ){
                                $(e).find("#menu").find("#"+groupId).find(".gallery").append($item);
                            };
                        });
                });

        }
        //Функция добавление картинки
        // Image recycle function
        var add_icon = "<button title='Добавить точку' class='ui-icon ui-icon-add' style='background-image:none;text-align:center'>+</button>";
        
        //Добавление в маршрут
        function addImage($item) {
            var parentId = $item.closest("a").closest("ul").parent("ul").attr("id");
            $item.draggable("disable").fadeOut(function() {
                var $list = $( "ul", $trash).length ?
                $( "ul", $trash ) :
                $( "<ul class='gallery ui-helper-reset' style='position: relative;top: -40px;padding: 0px;display: flex;margin: 0 auto;width: 100%;max-width: 100%;    overflow-y: -webkit-paged-x;'/>" )
                    .sortable({
                        items: "> li"
                    }).appendTo( $trash );
                
                
                
                //$trash.sortable("refresh")
                //console.log($trash)
                $item.find("button.ui-icon-add" ).remove();
                $item.find("#fotoPoint" ).css({"width" : "100%"})
                $item.find("#rightButton").append(trash_icon)
                
                //$item.append(trash_icon).appendTo( $list ).fadeIn(function() {
                var trashId = parentId.replace("gallery","trash");
                $item.appendTo($("#"+trashId + " ul")).fadeIn(function() {
                    $item.find( "img")
                });
            });
        }
        
        
        
        // Resolve the icons behavior with event delegation
        $( "ul.gallery > li" ).on( "click", function( event ) {
        var $item = $( this ),
            $target = $( event.target );
        
        if ( $target.is( "button.ui-icon-del" ) ) {
            deleteImage( $item );
        } else if ( $target.is( "button.ui-icon-refresh" ) ) {
            recycleImage( $item );
        } else if( $target.is("button.ui-icon-add")){
            addImage($item);
        }
        
        return false;
        });
        
        //функция добавления через Балун
        /*function addpoints(){
            
            
        }*/
        
        
        
        }
        
        
        //Функция рейтинга
        function rating(rating) {
            var ratingColorStarHTML = ''
            var ratingStarHTML = '<div id="ratingPointDefailColor"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></div>'
            if (rating == "1") {
                ratingColorStarHTML = ratingStarHTML + '<div id="ratingPointColor"><i class="far fa-star"></i></div>'
            } else if (rating == "2"){
                ratingColorStarHTML = ratingStarHTML + '<div id="ratingPointColor"><i class="far fa-star"></i><i class="far fa-star"></i></div>'
            } else if (rating == "3"){
                ratingColorStarHTML = ratingStarHTML + '<div id="ratingPointColor"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></div>'
            } else if (rating == "4"){
                ratingColorStarHTML = ratingStarHTML + '<div id="ratingPointColor"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></div>'
            } else if (rating == "5"){
                ratingColorStarHTML = ratingStarHTML + '<div id="ratingPointColor"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></div>'
            }
            return ratingColorStarHTML
        }
        
        
        //Преобразуем даты из полей ввода
        function toDate(dateStr) {
        var parts = dateStr.split(".")
        return new Date(parts[2], parts[1] - 1, parts[0])
        }
        

        
        //Проверка на заполнение
        function addAlert(fieldId){
            $("#"+fieldId).attr("placeholder", "Необходимо заполнить поле");
            $("#"+fieldId).addClass('allertPlaceholder');		
        }
        
        
        function deltaDataInOut(from,to){
            var arr = [];
            from = from.split(/-|\./);
            to = to.split(/-|\./);
            from = new Date(from[2],from[1]-1,from[0]);
            to = new Date(to[2],to[1]-1,to[0]);
            for (; from <= to; )  {
            arr.push((from.getDate()+"."+(from.getMonth()+1)+"."+from.getFullYear()).replace(/(^|\.)(?=\d\.)/g, "$10"));
            from.setDate(from.getDate()+1);
            }
            return arr
        }
        
        
        //Создает категории
        function creatCategory(groups){
            var catAdd = ''
            var categoryaHTML = ' <div class="categoryItems" id="group_'+groups.id+'">'+
                                    ' 		<div>'+
                                    '			<div id="iconCategory">'+
                                    '				<i style="width : 30px;height : 30px;" class="fas fa-utensils"></i>'+
                                    '			<div>'+
                                    '			<div style="position:relative">'+
                                    '				<span id="categoryCount">'+groups.items.length+'</span>'+
                                    '			</div>'+
                                    ' 		<div id="titleCategory">'+ groups.name +'<div>'+
                                    ' </div>'
            
            catAdd += categoryaHTML
            console.log(catAdd)
        }
        
        
